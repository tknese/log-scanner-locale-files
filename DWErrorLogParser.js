/*
Author: Jorge Matos (jorge.matos@callawaygolf.com)
Created Date: 7/11/2012
Script: DWErorrLogParser.js

Modified: 7/30/2012 - Added changes per Zlatin Zlatev 
	1) Added optional parameter "/do:" logDateOffset
	2) Changed "WebRequest" function to use Msxml2.ServerXMLHTTP to avoid having to mess with IE's security settings in regards to SSL certificates
	3) Code cleanup, if guards, comments, etc...

Modified: 10/2/2012 - Fixed bug with log date which prevented the log files from being processed.
	
Example: 
	cscript.exe .\DWErrorLogParser.js /s:https://production.web.XXX.demandware.net /u:servername /p:password /ef:fromemail@yourdomain.com /et:toemail@yourdomain.com /es:yoursmtpemailserver_ip_or_name /l:%APPDATA%\logs

Modified: 10/8/2019 - Travis Knese
	1) Commented out all email and webservice requests. This includes any username and passwords. Since this version is for locally host files. 
	2) Added functionality to use locally hosted files. So Production is not exposed directly.


Contributors:
	Zlatin Zlatev (zlatin.zlatev@ecommera.co.uk)
	Travis Knese (travis.knese@capgemini.com)
*/

SetupPrototypes();

Main();

function help() { 		
	"This script scans the log files for a given directory.".writeln();
	"".writeln();		
	"The following summary files will be created in the current script directory:".writeln();
	"	\"logs.zip\": all the log files compressed into a single zip".writeln();
	"	\"summary.htm\": an HTML file containing the summary of errors by website and pipeline".writeln();	
	"".writeln();			
	"The latest version of this script has been tested on windows 10. While previous versions were tested on windows 7, windows server 2003 and windows server 2008".writeln();
};

function usage() {
	"Usage: cscript.exe .\\DWParseErrorLogs.js /l:logdir  [/d:logdate] [/do:logdateoffset]".writeln();
	"".writeln();	
	"Download all logs that you want included in the sacn to a folder on your machine.".writeln();
	"Check the downloaded files in the folder for any that are 0MB and remove any found. These will cause the scanner to fail.".writeln();
	"Enusre that no files are in the folder other than logs to be scanned.".writeln();
	"/l: log directory path - where log files have been downloaded - Required".writeln();
	"Log Dates will be used as the date of the scan date of the scan in the Summary file generated".writeln();
	"/d: log date - optional - default is yesterday's date. This will be used in the naming of the files generated and date of the scan. Enter a value here to change the date in the report.".writeln();
	"/? help".writeln();
	"".writeln();
	"Example :".writeln();
	"This example scans all the logs in the targeted directory.".writeln();
	"cscript.exe /l:c:\logs".writeln();
	"".writeln();

	
};

function Main() {	
	var config = Config(), 
		fileManager = FileManager(),
		//logDownLoader = LogDownLoader(config, WebRequest()), 
		logDownLoader = LogDownLoader(config),
		logParser = LogParser();
		//mailer = Mailer(config.smtpServer);  

	(function () {				
		var currDir = fileManager.getAbsolutePathName("."),
			logDir = config.logDir && fileManager.getAbsolutePathName(config.logDir),
			//server = config.server,
			files = logDownLoader.list(),
			totalErrors = 0,
			summaryFilePath = fileManager.buildPath(currDir, "summary.htm"),			
			zipPath = fileManager.buildPath(currDir, "logs.zip");
			//emailTo = config.emailTo,
			//emailFrom = config.emailFrom;
				
/*		if (fileManager.folderExists(logDir)) {
			fileManager.deleteDir(logDir);
		};
		fileManager.makeDir(logDir);
*/		
		if (files.length) {
			"Downloading and Parsing Error Logs".writeln();
			files.forEach(function (fileName) {
				var fileContents, errorCount;
				(fileName + " ").write();
				//fileContents = logDownLoader.get(fileName);
				fileContents = fileManager.getLocalFile(fileName);
				errorCount = logParser.parse(fileName, fileContents);
				("(Errors: " + errorCount + ")").writeln();	
				totalErrors += errorCount;							
				fileManager.saveToFile(fileManager.buildPath(fileName), fileContents);			
			});		
					
			fileManager.saveToFile(summaryFilePath, logParser.summary(config));				
			("Total Errors: " + totalErrors).writeln();
			
			fileManager.zip(logDir, zipPath);
					
			/*if (emailFrom && emailTo) {
				mailer.sendMail({
					"from": emailFrom,
					"to": emailTo, 
					"subject": "Demandware Error Log Summary for " + config.logDate.toDateString(),
					"htmlFile":  summaryFilePath,
					"attachments": [zipPath]
				});
			};	
			*/

		}
		/*fileManager.deleteDir(logDir);
	*/		
	}).perf();	
};

function Config() {
	var //server = WScript.Arguments.Named.Item("s"), 
		//username = WScript.Arguments.Named.Item("u"), 
		//password = WScript.Arguments.Named.Item("p"), 
		logDate = WScript.Arguments.Named.Item("d"),
		logDateOffset = WScript.Arguments.Named.Item("do"),
		logDir = WScript.Arguments.Named.Item("l"),
		//emailFrom = WScript.Arguments.Named.Item("ef"),
		//emailTo = WScript.Arguments.Named.Item("et"),
		//smtpServer = WScript.Arguments.Named.Item("es"),
		today = new Date(),
		re = /^[-](\?|h)/g,
		invalidArguments,
		formattedLogDate;
	
	function pad(n) {
		return n < 10 ? "0" + n : "" + n;
	};	
	
	//invalidArguments = !server || !username || !password || !logDir;
	invalidArguments = !logDir;
		
	if ((WScript.Arguments.Unnamed.length > 0) && re.test(WScript.Arguments.Unnamed.Item(0))) {
		help();
		WScript.Quit(1);
	};
	
	if (WScript.Arguments.length == 0 || invalidArguments) {		
		"Invalid usage: Check all required parameters".writeln();		
		"".writeln();		
		usage();
		WScript.Quit(1);
	};			
	/*	
	if (!/^https?/i.test(server)) {
		"It looks like you forgot to add 'http' or 'https' to the beginning of the server name".writeln();
		WScript.Quit(1);
	};
	*/
	logDate = (logDate && new Date(logDate)) || new Date(today.getFullYear(), today.getMonth(), today.getDate() - (logDateOffset ? logDateOffset : 1));	
	formattedLogDate = String(logDate.getFullYear()) + pad((logDate.getMonth() + 1)) + pad(logDate.getDate());
		
	return {
		"logDate": logDate,
		"formattedLogDate": formattedLogDate,
		//"server": server,
		//"username": username,
		//"password": password,
		
		//"emailFrom": emailFrom,
		//"emailTo": emailTo,
		//"smtpServer": smtpServer
		"logDir": logDir
	};	
};

function LogParser() {
	var totalErrors = [];

	// Parse a log file and return an array of error objects 
	function parseLog(fileName, contents) {
		var errorRegex = /^\[([^\]]*)\]([^\|]*)\|([^\|]*)\|([^\|]*)\|([^\|]*)\|([^\|]*)\|(.*)$/gm,
			match, matches = [], errors = [];
		
		do {
			match = errorRegex.exec(contents);
			match && matches.push(match);
		} while (match);

		if (matches.length) {
			matches.forEach(function (m) {
				var website = m[4].trim(),
					pipeline = m[5].trim(),
					errorMessage = m[7].trim().replace(/\r/g, ""),
					sessionid = m[3].trim();				
				
				if (website.substr(0,6).toLowerCase() === "sites-") {					
					errors.push({
						"index": m.index,
						"match": m[0],
						"errorDate": m[1],
						"errorSource": m[2].trim(),
						"sessionid": sessionid,
						"website": website,
						"pipeline": pipeline,
						"errorMessage": errorMessage
					});
				} else {
					errors.push({
						"index": m.index,
						"match": m[0],
						"errorDate": m[1],
						"errorSource": m[2].trim(),
						"sessionid": "",
						"website": "unspecified",
						"pipeline": "unspecified",
						"errorMessage": m[0]
					});
				};
			});

			errors.forEach(function (error, index) {
				var start, length;
				start = error.index + error.match.length;
				length = 0;

				if ((index + 1) < errors.length) {
					length = errors[index + 1].index - start;
				} else {
					length = contents.length;
				};

				if (error.website === "unspecified") {
					error.errorKey = error.errorMessage.substr(0, 500);
				} else {
					error.errorDetail = contents.substr(start, length).trim().replace(/\r$/g, "");
					if (error.errorDetail.substr(0,6) !== "Trace:") {
						error.errorDetail = error.errorDetail.substr(error.errorDetail.indexOf("\n")+1);
					};
					
					error.errorKey = fileName.indexOf("custom") !== -1 ? error.errorMessage.substr(error.errorMessage.indexOf("custom")).trim() : error.errorMessage.substring(error.errorMessage.indexOf("\"")).trim();
					error.errorKey += " " + error.errorDetail.substring(0, error.errorDetail.indexOf("\n")-1);
				};
										
				delete error.index;
				delete error.match;
			});
		};

		return errors;
	};

	// Generates the "summary" data, which consists of the total errors per website and for each
	// website the total errors per pipeline
	function summarizeErrorsByWebSite(errors) {
		var sortedErrors, summary = "",
			prevWebSite, countWebSite, totalErrorCount, results = [];

		sortedErrors = errors.sortObjects("website");
		prevWebSite = sortedErrors[0] ? sortedErrors[0].website : "unspecified";
		countWebSite = 0;
		totalErrorCount = 0;

		sortedErrors.forEach(function (ele) {
			if (ele.website !== prevWebSite) {
				results.push({
					"website": prevWebSite,
					"count": countWebSite
				});
				totalErrorCount += countWebSite;
				countWebSite = 1;
				prevWebSite = ele.website;
			} else {
				countWebSite++;
			};
		});

		totalErrorCount += countWebSite;
		results.push({
			"website": sortedErrors[sortedErrors.length - 1] ? sortedErrors[sortedErrors.length - 1].website : "unspecified",
			"count": countWebSite
		});

		//Sort websites by error count descending	
		results = results.sortObjects("count", true);
				
		results.forEach(function (ele) {
			var currWebSite = ele.website;		
			summary += "<ul>";
			summary += "<li><strong>" + ele.count + "</strong>&nbsp;" + currWebSite + "</li>";			
			summary += "<ul>";			
			summary += summarizeErrorsByPipeline(sortedErrors.filter(function (ele) {
				return ele.website === currWebSite;
			}));		
			summary += "</ul>";			
			summary += "</ul>";
		});
				
		return summary;
	};

	// This function is called by the "summarizeErrorsByWebSite" function to produce the list of 
	// pipelines for a specific website and the count of errors for each pipeline.
	// The "errors" parameter is assumed to be a subset of the "errors" array produced by filtering 
	// on the "website" property.
	function summarizeErrorsByPipeline(errors) {
		var sortedErrors, summary = "",
			prevPipeline, countPipeline, results = [];

		sortedErrors = errors.sortObjects("pipeline");
		prevPipeline = sortedErrors[0] ? sortedErrors[0].pipeline : "unspecified";
		countPipeline = 0;

		sortedErrors.forEach(function (ele) {
			if (ele.pipeline !== prevPipeline) {
				results.push({
					"pipeline": prevPipeline,
					"count": countPipeline
				});
				countPipeline = 1;
				prevPipeline = ele.pipeline;
			} else {
				countPipeline++;
			};
		});

		results.push({
			"pipeline": sortedErrors[sortedErrors.length - 1] ? sortedErrors[sortedErrors.length - 1].pipeline : "unspecified",
			"count": countPipeline
		});

		//Sort pipelines by error count descending
		results = results.sortObjects("count", true);

		results.forEach(function (ele) {
			var currPipeline = ele.pipeline;		
			summary += "<ul>";
			summary += "<li><strong>" + ele.count + "</strong>&nbsp;" + currPipeline + "</li>";			
			summary += "<ul>";			
			summary += summarizeErrorsByErrorKey(sortedErrors.filter(function (ele) {
				return ele.pipeline === currPipeline;
			}));
			summary += "</ul>";			
			summary += "</ul>";
		});

		return summary;
	};

	// This function is called by the "summarizeErrorsByWebSite" function to produce the list of 
	// pipelines for a specific website and the count of errors for each pipeline.
	// The "errors" parameter is assumed to be a subset of the "errors" array produced by filtering 
	// on the "website" property.
	function summarizeErrorsByErrorKey(errors) {
		var sortedErrors, summary = "",
			prevError, lastError, countError, results = [];

		sortedErrors = errors.sortObjects("errorKey");
		prevError = sortedErrors[0] ? sortedErrors[0].errorKey : "unspecified";
		countError = 0;

		sortedErrors.forEach(function (ele) {
			var currError = ele.errorKey;
			if (currError !== prevError) {
				results.push({
					"error": prevError,
					"count": countError
				});
				countError = 1;
				prevError = currError;
			} else {
				countError++;
			};
		});
		
		lastError = sortedErrors[sortedErrors.length - 1] ? sortedErrors[sortedErrors.length - 1].errorKey : "unspecified";
		results.push({
			"error": lastError,
			"count": countError
		});
		
		//Sort errors by error count descending
		results = results.sortObjects("count", true);

		results.forEach(function (ele, index) {			
			summary += "<li><div" + (index % 2 === 0 ? " style='background-color:lightgray'>" : ">") + "<strong>" + ele.count + "</strong>&nbsp;" + ele.error + "</div></li>";			
		});

		return summary;
	};
	
	return {
		"parse": function(fileName, fileContents) {			
			var errors = parseLog(fileName, fileContents);
			errors.forEach(function (ele) {
				totalErrors.push(ele);
			});
			return errors.length;
		},
		"summary": function(config) {
			var summary = "";						
			summary += "<html><body>";	
			//summary += "Server: <strong>" + config.server + "</strong></br>";
			summary += "Log Date: <strong>" + config.logDate.toDateString() + "</strong></br>";
			summary += "Total Errors: <strong>" + totalErrors.length + "</strong>";			
			summary += summarizeErrorsByWebSite(totalErrors)
			summary += "</body></html>";
			delete totalErrors;			
			return summary;
		}
	};
}
/* function Mailer(smtpServer) {
	/*
	This function sends emails.
	Example:
		var mailer = Mailer("email.server.com");  

		mailer.sendMail({
			"from": "DemandwareErrorLogs@yourcompany.com",
			"to": "jorge.matos@callawaygolf.com",  
			"subject": "Sending Error Log Email",
			"htmlFile": "file://C:\\Users\\jorge.matos\\Desktop\\summary.htm",
			"attachments": ["C:\\Users\\jorge.matos\\Desktop\\logs.zip"]
		});

		WScript.Echo("Email Sent!");
	
	return {
		
		options = {
			"from": "email from address" 
			"to": "email to address"
			"textBody": "body text as string"
			"htmlBody": "HTML string"
			"htmlFile": "email body is created from url to HTML file - i.e. file://C:\\message.htm"
			"attachments": "array of filepaths strings to attach to the email"
		}
		
		"sendMail": function (options) {
			var myMail = new ActiveXObject("CDO.Message");
			
			myMail.From = options.from;
			myMail.To = options.to;
			myMail.Subject = options.subject;	
			
			if (options.htmlBody) {		
				myMail.HTMLBody = options.htmlBody;	
			} else if (options.htmlFile) {	
				myMail.CreateMHTMLBody("file://" + options.htmlFile);
			} else {
				myMail.TextBody = options.textBody
			};
			
			if (options.attachments) {
				options.attachments.forEach(function (filePath) {
					myMail.AddAttachment(filePath);
				});				
			};
			
			myMail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2;
			//Name or IP of remote SMTP server
			myMail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = smtpServer;
			myMail.Configuration.Fields.Update();
			myMail.Send();
		}
	};
};
*/
function FileManager() {
	var ForReading= 1, ForWriting = 2, TristateUseDefault = -2, fso = new ActiveXObject("Scripting.FileSystemObject");
	
	return {		
		"saveToFile": function (filePath, contents) {
			var fileObj, ts;
	
			fso.CreateTextFile(filePath);
			
			fileObj = fso.GetFile(filePath);

			// Open a text stream for output.
			ts = fileObj.OpenAsTextStream(ForWriting, TristateUseDefault);

			// Write to the text stream.
			ts.Write(contents);
			ts.Close();		
		},
		"folderExists": function(dir) {
			return fso.FolderExists(dir);
		},
		"getLocalFile": function(fileName) {
			var localFile = fso.OpenTextFile(fileName, 1);
			var localContent = localFile.ReadAll();
			return localContent;
		},
		"deleteDir": function(dir) {
		/*	var file, enumerator;	
			if (fso.FolderExists(dir)) {				
				enumerator = new Enumerator(fso.getFolder(dir).files);
				for (; !enumerator.atEnd(); enumerator.moveNext()) {
					file = enumerator.item();					
					file.Delete();					
				};

				fso.DeleteFolder(dir);
			};
		*/		
		},
		"makeDir": function(dir) {
			fso.CreateFolder(dir);
		},
		"getAbsolutePathName": function(path) {
			return fso.GetAbsolutePathName(path);
		},
		"buildPath": function(dir, fileName) {
			return fso.BuildPath(dir, fileName);
		},
		"zip": function zip(srcDir, targetZip) {	
			var winShell = new ActiveXObject("shell.application"), 
				folder, file, ts, zipHeader = "";
			
			[80,75,5,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0].forEach(function (ele) {
				zipHeader += String.fromCharCode(ele);
			});
				
			fso.CreateTextFile(targetZip);
			file = fso.GetFile(targetZip);
			ts = file.OpenAsTextStream(ForWriting, TristateUseDefault);
			ts.write(zipHeader);
			ts.close();
				
			winShell.NameSpace(targetZip).CopyHere(winShell.NameSpace(srcDir).Items());
			WScript.sleep(2000);
		}	
	};
};

function SetupPrototypes() {
	if (typeof String.prototype.writeln === "undefined") {
		String.prototype.writeln = function () {
			WScript.Echo(this);
		};
	};
	
	if (typeof String.prototype.write === "undefined") {
		String.prototype.write = function () {
			WScript.StdOut.Write(this);
		};
	};

	if (typeof String.prototype.trim === "undefined") {
		String.prototype.trim = function () {
			// Trim whitespace from beginning and end of the string    
			return this.replace(/^\s*/, "").replace(/\s*$/, "");
		};
	};

	if (typeof Array.prototype.forEach === "undefined") {
		Array.prototype.forEach = function (f) {
			// This function iterates through an array and calls a function for each element in the array    
			var index, result, limit = this.length;
			for (index = 0; index < limit; index++) {
				result = f(this[index], index, this);
				//Exit if the function "f" returned a "truthy" value
				if (result) {
					return;
				};
			};    
		};
	};

	if (typeof Array.prototype.filter === "undefined") { 
		Array.prototype.filter = function (f) {
			// This function produces a new array by iterating through a source array and calling a function
			// for each element and using the result to determine if it should add the element to the new array	
			var a = [];
			this.forEach(function (ele) {
				if (f(ele)) {
					a.push(ele);
				};
			});
			return a;
		};
	};

	if (typeof Array.prototype.sortObjects === "undefined") { 
		Array.prototype.sortObjects = function (property, desc) {
			// Sorts an array of objects based on a specific object property     
			var returnArray = this.slice(0);

			returnArray.sort(function (p1, p2) {
				var returnValue,
				first = p1[property],
				second = p2[property];

				if (first === second) {
					returnValue = 0;
				} else if (first < second) {
					returnValue = -1;
				} else {
					returnValue = 1;
				};
				return desc ? (returnValue * -1) : returnValue;
			});

			return returnArray;
		};
	};	

	if (typeof Function.prototype.perf === "undefined") {
		// Display the processing time for the execution of a function 
		Function.prototype.perf = function (msg) {		
			var f = this, args = Array.prototype.slice.call(arguments, 1);		
			return (function() {
				var start = new Date(), end;
				try {				
					return f.apply(f, args);
				} finally {
					end = new Date();
					((msg || "Total Elapsed Time: ") + (end - start) + " ms").writeln();
				};
			})();
		};
	};
};

//function LogDownLoader(config, webRequest) {
function LogDownLoader(config) {	
	//var username = config.username,
		//password = config.password;
	var logDir1 = config.logDir;
	return {
		"list": function () {
			//var url = config.server + "/on/demandware.servlet/webdav/Sites/Logs",				
			var	fso, 
			fso = new ActiveXObject("Scripting.FileSystemObject"),
			objShell = new ActiveXObject("Shell.Application"),
			files = [],
			logDir1 = config.logDir,
						
			enumerator = new Enumerator(fso.getFolder(logDir1).files);
			for (; !enumerator.atEnd(); enumerator.moveNext()) {
				var file = enumerator.item();
					
				files.push(file.path);	
			};
						
				//rawData = webRequest.get(url, username, password),
				//re = /\<a href="\/([^\/]*)\/([^\/]*)\/([^\/]*)\/([^\/]*)\/([^\/]*)\/(.*error\-blade.*.log)"/gm,
				//matches = [],
				//match;
				
			/*
			do {
				match = re.exec(rawData);
				match && ("Found log file: "+match[6]).writeln();
				match && matches.push(match);
			} while (match);

			if (matches.length) {
				matches.forEach(function (m) {					
					var fileName = m[6];
					if (fileName.indexOf(config.formattedLogDate) !== -1) {
						("Adding log file for processing: "+m[6]).writeln();
						files.push(m[6]);
					};
				});

			}
			*/
			if (!files.length) {
				"no files found".writeln();
			};
			
			
			

			
			
			return files;
		},
		"get": function (fileName) {
			//var url = config.server + "/on/demandware.servlet/webdav/Sites/Logs/" + fileName;
			//return  webRequest.get(url, username, password);	
				return fileManager.getLocalFile(fileName);
		}
	};
};

/*
function WebRequest() {
	return { 
		"get": function(url, username, password) {				
			var xmlhttp = new ActiveXObject("Msxml2.ServerXMLHTTP");
			var SXH_SERVER_CERT_IGNORE_ALL_SERVER_ERRORS = 13056;
			xmlhttp.setOption(2, SXH_SERVER_CERT_IGNORE_ALL_SERVER_ERRORS);
			xmlhttp.open("GET", url, false, username, password);			
			xmlhttp.send();
			if (xmlhttp.readyState === 4) {
				if (xmlhttp.status === 200) {					
					return xmlhttp.responseText;
				};
			};
		}
	};
};
*/
