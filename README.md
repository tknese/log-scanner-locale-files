# Log Scanner-Locale Files

This is a modified version of a log scanner found on xchange https://xchange.demandware.com/docs/DOC-11230.
This version uses locally hosted files. This is done to reduce potential security issues using the previous version of the scanner on a production site. Also when trying to use the previous version on a production site it timed out due to numerous different logfiles in the logs directory. Due to many different integratiosn and 14 active blades. Which cause the scanner to fail and was unusable.

About:

This script scans the log files for a given directory.
The following summary files will be created in the current script directory:
\"logs.zip\": all the log files compressed into a single zip 
\"summary.htm\": an HTML file containing the summary of errors by website and pipeline
The latest version of this script has been tested on windows 10. While previous versions were tested on windows 7, windows server 2003 and windows server 2008


How To Use: 

Download all logs that you want included in the sacn to a folder on your machine.
Check the downloaded files in the folder for any that are 0MB and remove any found. These will cause the scanner to fail.
Enusre that no files are in the folder other than logs to be scanned.
Open Commandline or equvialnt program. Then change the directory to the one that contains DWParseErrorLogs.js.
For example: cd  C:\logscanner  
Then enter in command line cscript.exe .\\DWParseErrorLogs.js /l:logdir  [/d:logdate] 
/l: log directory path - where log files have been downloaded - Required
Log Dates will be used as the date of the scan date of the scan in the Summary file generated
/d: log date - optional - default is yesterday's date. This will be used in the naming of the files generated and date of the scan
To change the date enter in command line cscript.exe .\\DWParseErrorLogs.js /l:logdir  /d:logdate
/? help

Example :
This example scans all the logs in the targeted directory.
cscript.exe /l:c:\logs
